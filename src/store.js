import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: true,

  state: {
    sum: 0,
    controls: [
      {
        value: 111,
        enableEdit: false
      },
      {
        value: 222,
        enableEdit: false
      },
      {
        value: 333,
        enableEdit: false
      }
    ],
    showClickableArea: false,
    tabIndexCurrent: 0,
    tabIndexTotal: null,
    counter: 0
  },
  mutations: {
    SET_ENABLE_EDIT: (state, payload) => {
      state.controls[payload.control].enableEdit = payload.value
    },
    SET_INPUT_VALUE: (state, payload) => {
      state.controls[payload.id].value = payload.value
    },
    SET_CLICKABLE_AREA: (state, value) => {
      state.showClickableArea = value
    },
    SET_TAB_INDEX_CURRENT: (state, index) => {
      state.tabIndexCurrent = index
    },
    SET_TAB_INDEX_TOTAL: (state, value) => {
      state.tabIndexTotal = value
    },
    SET_COUNTER: (state, value) => {
      state.counter += value
    }
  },
  getters: {
    getControls: state => {
      return state.controls
    },
    getClickableArea: state => {
      return state.showClickableArea
    },
    getTabIndex: state => {
      return state.tabIndexCurrent
    },
    getTabIndexTotal: state => {
      return state.tabIndexTotal
    }
  },
  actions: {
    sum({ state, commit }) {
      const value1 = parseInt(state.controls[1].value)
      const value2 = parseInt(state.controls[2].value)

      Promise.all([value1, value2])
        .then(values => {
          const value = values.reduce((accumulator, currentValue) => {
            return accumulator + currentValue
          }, 0)

          const id = 0
          commit('SET_INPUT_VALUE', { id, value })
        })
    },
    outsideThisControl(context, e) {
      const target = e.target
      const allContainer = document.querySelectorAll('.input-container')
      const result = []

      allContainer.forEach((flyoutElement) => {
        let targetElement = target
        do {
          if (targetElement === flyoutElement) {
            result.push(true)
            return
          }
          targetElement = (targetElement.parentNode) ? targetElement.parentNode : null
        } while (targetElement)
        result.push(false)
      })

      if (!result.includes(true)) {
        this.dispatch('onClickOutside')
      }
    },
    onClickOutside({ state, commit }) {
      state.controls.forEach((item, index) => {
        commit('SET_ENABLE_EDIT', { control: index, value: false })
        commit('SET_CLICKABLE_AREA', true)
        setTimeout(() => {
          commit('SET_CLICKABLE_AREA', false)
        }, 1200)
      })
    },
    disableAllControls({ state, commit }) {
      state.controls.forEach((item, index) => {
        commit('SET_ENABLE_EDIT', { control: index, value: false })
      })
    },
    getControlValue({ state }, id) {
      return new Promise(resolve => {
        resolve(state.controls[id].value)
      })
    },
    setDefaultValue({ commit }) {
      const value = 1000
      const id = 1
      commit('SET_INPUT_VALUE', { id, value })
    }
  }
})
